(ns aoc.day10
  (:require [clojure.string :as str]
            [clojure.java.io :as io]))

(def demo-input [16 10 15 5 1 11 7 19 6 12 4])

(def demo-input-2 [28 33 18 42 31 14 46 20 48 47 24 23 49 45 19 38 39 11 1 32 25 35 8 17 7 9 4 2 34 10 3])

(def actual-input (->> (slurp (io/resource "input_day10.txt"))
                       str/split-lines
                       (map #(Integer/parseInt %))))

(defn deltas [input]
  (->> input
       (apply max)
       (+ 3)
       (conj input 0)
       sort
       (partition 2 1)
       (mapv (fn [[a b]] (- b a)))))

(defn solve-part01 []
  (let [{ones 1, threes 3} (frequencies (deltas actual-input))]
    (* ones threes)))

(solve-part01)

(defn print-solutions []
  (println "Solution part01:" (solve-part01)))

(defn -main [&args]
  (print-solutions))

(comment
  (deltas demo-input)               ;;  [1 3 1 1 1 3 1 1 3 1 3 3
  (frequencies (deltas demo-input)) ;; {1 7, 3 5}
  )