(ns aoc2020.day11
  (:require [clojure.string :as str]
            [clojure.java.io :as io]))

(defn read-input [filename]
  (->> (slurp (io/resource filename))
       str/trim
       str/split-lines
       (map #(map char %))))

(def actual-input (read-input "input_day11"))
(def test-input-all-empty (read-input "input_day11_test01"))
(def test-input-all-full (read-input "input_day11_test02"))

(defn debug-print-layout [layout]
  (mapv #(println (apply str %)) layout)
  (println))

;; L - empty seat
;; . - floor
;; # - occupied seat

(defn seat [layout [x y]]
  (let [height (count (first layout))
        width (count layout)]
    (cond (< x 0) nil
          (< y 0) nil
          (>= x width) nil
          (>= y height) nil
          :else (let [row (nth layout x)
                      col (nth row y)]
                  col))))

(defn occupied-count [layout [sx sy]]
  (->> (for [x (range (dec sx) (+ 2 sx))
             y (range (dec sy) (+ 2 sy))
             :when (not= [x y] [sx sy])]
         [x y])
       (map #(seat layout %))
       (filter #(= \# %))
       count))

(defn next-seat-state
  "Determines next state of a given seat according to the given rules

  Rules
  if a set is empt
  If a seat is empty (L) and there are no occupied seats adjacent to it, the seat becomes occupied.
  If a seat is occupied (#) and four or more seats adjacent to it are also occupied, the seat becomes empty.
  Otherwise, the seat's state does not change.
  "
  [layout [sx sy]]
  (let [seat (seat layout [sx sy])
        oc (occupied-count layout [sx sy])]
    (cond (= seat \.) \.
          (and (= seat \L) (= 0 oc)) \#
          (and (= seat \#) (>= oc 4)) \L
          :else seat)))


(defn next-layout-state [layout]
  (let [width (count (first layout))
        height (count layout)
        seats (for [x (range 0 width) y (range 0 height)] [x y])]
    (partition width (map #(next-seat-state layout %) seats))))

(defn count-occoupied [layout]
  (count (filter #(= \# %) (flatten layout))))

(defn simulate-till-no-change [layout]
  (loop [state layout]
    (let [next-state (next-layout-state state)]
      (if (= state next-state)
        next-state
        (recur next-state)))))

;; (count-occoupied (simulate-till-no-change test-input-all-empty))

(->> (next-layout-state test-input-all-empty)
     next-layout-state
     next-layout-state
     next-layout-state
     next-layout-state
     debug-print-layout)

(println (count-occoupied (simulate-till-no-change actual-input)))

(defn solve-part01 []
  (let [layout actual-input]))

(defn print-solution []
  (println "Solution part01:" (solve-part01)))
