(ns aoc2020.day12
  (:require [clojure.string :as str]
            [clojure.java.io :as io]))

(defn parse-action [s]
  (let [[_ dir val] (re-find #"(.)(\d+)" s)
        val (Integer/parseInt val)]
    (cond (= dir "N") {:action :north :val val}
          (= dir "S") {:action :south :val val}
          (= dir "E") {:action :east :val val}
          (= dir "W") {:action :west :val val}
          (= dir "L") {:action :left :val val}
          (= dir "R") {:action :right :val val}
          (= dir "F") {:action :forward :val val}
          :else nil)))

(defn read-input []
  (->> (slurp (io/resource "input_day12"))
       str/split-lines
       (map #(parse-action %))))

(def test-input
  (->> "F10\nN3\nF7\nR90\nF11"
       str/split-lines
       (map #(parse-action %))))

test-input

(def actual-input (read-input))


;; Action N means to move north by the given value.
;; Action S means to move south by the given value.
;; Action E means to move east by the given value.
;; Action W means to move west by the given value.
;; Action L means to turn left the given number of degrees.
;; Action R means to turn right the given number of degrees.
;; Action F means to move forward by the given value in the direction the ship is currently facing.

(defn turn-left [location degree]
  (let [direction (:facing location)]
    (cond (= direction :north) location)))

(defn turn-right [location degree]
  ;; TODO: Implement me
  )

(defn navigate [actions]
  (loop [idx 0
         position {:location [0 0] :facing :east}]
    (let [action (nth actions idx)
          [x y] (:location position)
          n (:val action)]
      (condp :action action
        :north (recur (inc idx) (assoc position :location [x (+ y n)]))
        :south (recur (inc idx) (assoc position :location [x (- y n)]))
        :east  (recur (inc idx) (assoc position :location [(+ x n) y]))
        :west  (recur (inc idx) (assoc position :location [(- x n) y]))
        :left  (recur (inc idx) (assoc position :facing (turn-left position n)))
        :right (recur (inc idx) (assoc position :facing (turn-right position n)))
        (str "invalid action" action)))))

