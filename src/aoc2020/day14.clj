(ns aoc.day14
  (:require [clojure.string :as str]
            [clojure.java.io :as io]))


(defn read-input []
  (->> (slurp (io/resource "input_day14"))))

(def test-input
  (->> "mask = XXXXXXXXXXXXXXXXXXXXXXXXXXXXX1XXXX0X\nmem[8] = 11\nmem[7] = 101\nmem[8] = 0"
       str/split-lines))

(defn line->mask [s]
  (let [[_ mask] (re-find #"mask = (\w+)" s)]
    mask))

(defn line->adrval [s]
  (let [[_ addr val] (re-find #"mem\[(\d+)\] = (\d+)" s)]
    [(Integer/parseInt addr) (Integer/parseInt val)]))

(line->adrval (second test-input))

(->> (line->mask (first test-input))
     (#(str/replace % #"X" "1"))
     _(#(Integer/parseInt % 2))
     _(bit-or 8))

(defn binary [n]
  (Long/toBinaryString n))

(let [num 101
      strmask (line->mask (first test-input))
      zero-mask (Long/parseLong (str/replace strmask #"X" "0") 2)
      one-mask (Long/parseLong (str/replace strmask #"X" "1") 2)
      inv-one-mask (bit-not one-mask)]
  (println (.intValue inv-one-mask))
  (->> (bit-or zero-mask num)
       #_(#(- % (.intValue inv-one-mask)))))

;; The current bitmask is applied to values immediately before they are written to memory:
;; a 0 or 1 overwrites the corresponding bit in the value,
;; while an X leaves the bit in the value unchanged. 

;; --> X = AND
;; 0,1 = XOR

value:  0001011  (decimal 11)
mask:   1XXXX0X
result: 1001001  (decimal 73)

value:  1100101  (decimal 101)
mask:   1XXXX0X
result: 1100101  (decimal 101)

value:  0000000  (decimal 0)
mask:   1XXXX0X
result: 1000000  (decimal 64)